'use strict';
 
const express = require('express');
const app = express();

const mysql = require('mysql2');

// Prometheus client for counter and default metrics
const prom  = require('prom-client');
const numSiteVisits = new prom.Counter({
  name: 'jw_counter_app_site_visits',
  help: 'Number of times Increment and Decrement Counter App content webpages are visited'
});

const PORT = 80;
const HOST = '0.0.0.0';

const nameFromEnvVar = process.env.JW_NAME;

// Interact with MySQL and load page with Create Table, Insert & Select query results

var connection = mysql.createConnection({
    host: "mysql-service",
    user: "counter_user",
    password: "password",
    database: "counter_db"
});

var createTableResult = "Failed to create Counters table";
connection.query(
  "CREATE TABLE IF NOT EXISTS Counters (id INT AUTO_INCREMENT PRIMARY KEY, number INT)",
  function(err, results) {
    if (err) throw err;
    createTableResult = "Counters table creation passed";
  }
);

var insertResult = "Failed to Insert row";
connection.query(
  `INSERT INTO Counters (number) VALUES (\'${Math.random() * 1000}\')`,
  function(err, results) {
    if (err) throw err;
    insertResult = "New row inserted";
  }
);

var selectResult = "Failed Select query";
connection.query(
  'SELECT * FROM Counters',
  function(err, results, fields) {
    if (err) throw err;
    selectResult = "Select query returned:"
    Object.keys(results).forEach(function(key) {
      var row = results[key];
      selectResult += ` [${row.id}, ${row.number}]`
    });
  }
);

// Routing

app.engine('html', require('ejs').renderFile);

app.use('/', express.static('views'));

app.get('/', function(req, res) {
  res.render("counter-app.html", {nameFromEnvVar:nameFromEnvVar});
  numSiteVisits.inc();
});

app.get('/mysql', function(req, res) {
  res.render("mysql-tracker.html", {createTableResult:createTableResult, insertResult:insertResult, selectResult:selectResult});
  numSiteVisits.inc();
});

// Metrics and metrics endpoint
const collectDefaultMetrics = prom.collectDefaultMetrics;
collectDefaultMetrics();
app.get('/metrics', async function (req, res) {
  res.set('Content-Type', prom.register.contentType);
  res.end(await prom.register.metrics());
});

app.listen(PORT, HOST, () => {
  console.log(`Running on http://${HOST}:${PORT}`);
});

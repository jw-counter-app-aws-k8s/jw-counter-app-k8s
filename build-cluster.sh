#!/bin/sh

docker build -t counter-app counter-app-code/
helm install counter-app counter-cluster-chart

kubectl create namespace monitoring
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update
helm install prometheus prometheus-community/prometheus -n monitoring
helm install grafana grafana/grafana -n monitoring

kubectl expose service prometheus-server --type=NodePort --target-port=9090 --name=prometheus-server-external -n monitoring
kubectl expose service grafana --type=NodePort --target-port=3000 --name=grafana-external -n monitoring

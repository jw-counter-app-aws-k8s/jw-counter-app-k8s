# Counter App in Kubernetes

## Purpose
Create a Kubernetes cluster that runs an "Increment and Decrement Counter" NodeJS web app, a MySQL server, and has monitoring with Grafana and Prometheus.

## Assumptions
1. All K8s app resources explicitly created here are in `default` namespace. All monitoring resources are in `monitoring` namespace.
2. MySQL server with persistent data across relaunches until cluster is destroyed or reset.
    - MySQL deployment using Persistent Volume Claim is created with Helm. Helm does not remove such volumes when deleting its resources, so subsequent Helm reinstalls of the MySQL server will have the same data. Volumes removed after K8s cluster stops.
3. Developed on local single-node Kubernetes cluster. Should work on clusters with multiple nodes.
4. Developed on Windows Docker Desktop Kubernetes cluster. Should work on any cluster, such as Minikube or cloud K8s.

## Instructions

### Pre-requisites
1. `docker` and `helm` installed on code execution machine.
2. A Kubernetes cluster with `kubectl` configured to interact with the cluster.

### Cluster Setup Steps
1. From root directory, run `build-cluster.sh` script.
    - **Keep this terminal window open to refer to installation outputs later. Open another terminal tab/window to use in  subsequent steps.**
2. The following are created:
    1. **Docker image** `counter-app` for Increment and Decrement Counter NodeJS web app.
    2. **Increment and Decrement Counter web app Deployment** with 3 replicas and **NodePort 30080 Service**. Deployment has **initContainer** to check that MySQL server is up before initializing app.
    3. **MySQL Deployment** with 1 replica, **ClusterIP Service** and **Persistent Volume + Persistent Volume Claim**. A user and `Counter` database are created.
    4. **prometheus** and **grafana** Helm chart resources in `monitoring` namespace.
        - [Prometheus Helm](https://github.com/prometheus-community/helm-charts/)
        - [Grafana Helm](https://github.com/grafana/helm-charts)
    5. **NodePort Services to expose Grafana and Prometheus servers**.
3. **Increment and Decrement Counter web app** communicates with the MySQL server and exports Prometheus metrics. Visit the following URLs in browser:
    1. `localhost:30080` is the Increment and Decrement Counter main page. The author's name displayed is retrieved from app container spec's environment variable. [Screenshot](images/1.%20App%20Main%20Page%20Screenshot.png).
    2. `localhost:30080/mysql` is a a page that displays success or failure messages for MySQL queries `CREATE TABLE IF EXISTS`, `INSERT` and `SELECT` that are run on app startup. [Screenshot](images/2.%20App%20MYSQL%20Tracker%20Page%20Screenshot.png).
    3. `localhost:30080/metrics` is where Prometheus client metrics are exported to for scraping.
    4. App tracks and exports number of visits for content pages `/` and `/mysql` using Prometheus client `counter`, with metric name `jw_counter_app_site_visits`.

### Monitoring
1. Run `kubectl get services -n monitoring`. Note down `grafana-external` and `prometheus-server-external` Services' exposed ports (port after colon).
2. On browser, go to `localhost:[service-port]` using service ports from previous step to access Grafana and Prometheus dashboards.
3. Get Grafana admin password: Run `kubectl get secret --namespace monitoring grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo`.
4. Log in to Grafana Dashboard with user `admin` and password from previous step.
5. Refer to output of 1st terminal window that you ran `build-cluster.sh` on in the 1st ever step. Note down **internal DNS name and port of Prometheus server**. On a local cluster, it is usually `http://prometheus-server.monitoring.svc.cluster.local:80`.
6. Add Prometheus data source in Grafana Dashboard using the DNS name and port from the previous step, `http://[address]:[port]`.
7. Carry out monitoring: Add and use folders and dashboards in Grafana, use Prometheus dashboard to check targets and queries etc.
    - Some of the good Grafana imported dashboards tried:
        1. Pods: `17149`, `15760`, `6781`
        2. Cluster: `1621`, `15757`
        3. Nodes: `15759`
        4. Namespaces: `15758`
        5. NodeJS: `11159`
        6. Prometheus overview: `19105`
8. On Prometheus dashboard or Grafana Explore, query `jw_counter_app_site_visits` to see the site visits counter exported by Counter App. The values for the Pods will total up to the number of times `/` and `/mysql` pages are visited. This can be used to build analytic queries and dashboard components.
    - It may take a little time for Prometheus to scrape and update the metrics after a visit.
9. Potential Enhancements:
    - Use Prometheus client `counters` and `gauges` to get metrics of clicks and values on the Increment and Decrement buttons.
    - Monitor MySQL with `mysql-exporter`.

#### Monitoring Issues
1. `prometheus-prometheus-node-exporter` Helm install may fail to start on Docker Desktop. Fix is [here](https://github.com/prometheus-community/helm-charts/issues/467#issuecomment-957091174), however it may cause issues with node-exporter metrics.

### Cluster Teardown
Run `teardown-cluster.sh` from root directory to delete all resources created in these steps.

Note that MySQL persistent volumes are not deleted by Helm and will persist across reruns of these steps unless K8s cluster is recreated, as mentioned in [Assumptions](assumptions) section.

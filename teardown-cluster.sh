#!/bin/sh

helm uninstall counter-app
helm uninstall prometheus -n monitoring
helm uninstall grafana -n monitoring
kubectl delete service prometheus-server-external -n monitoring
kubectl delete service grafana-external -n monitoring
kubectl delete namespace monitoring

docker image rm counter-app
